using System;
using UnityEngine;
using Object = UnityEngine.Object;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Valhalla.EveryDay.Helpers
{
	public static class InstanceHelper
	{
		/// <summary>
		/// Instantiates prefab with preserving source asset link if possible (if not in runtime). 
		/// </summary>
		/// <param name="prefabObject">Prefab game object or component</param>
		/// <param name="position">Global position of created instance</param>
		/// <param name="rotation">Rotation of created instance</param>
		/// <param name="parent">Parent transform to instantiate to, can be null</param>
		/// <typeparam name="TObject">Type of component (or GameObject)</typeparam>
		/// <returns>Instantiated object or it's component</returns>
		/// <exception cref="NullReferenceException">Raised if <see cref="prefabObject"/> or instantiation result is null</exception>
		public static TObject InstantiatePrefab<TObject>(TObject prefabObject, Vector3 position, Quaternion rotation, Transform parent = null)
			where TObject : Object
		{
			if (prefabObject == null)
				throw new NullReferenceException("Can't instantiate null");
			
			#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				var result = PrefabUtility.InstantiatePrefab(prefabObject, parent);
				
				if (result == null)
					throw new NullReferenceException("Instantiation result is null");

				GameObject obj = typeof(TObject) == typeof(GameObject)
					? result as GameObject
					: ((MonoBehaviour)result).gameObject;
				
				obj.transform.SetParent(parent);
				obj.transform.position = position;
				obj.transform.rotation = rotation;

				return result as TObject;
			}
			#endif
			
			return Object.Instantiate(prefabObject, position, rotation, parent);
		}
	}
}
