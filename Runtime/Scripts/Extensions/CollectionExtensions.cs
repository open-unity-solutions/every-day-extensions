using System;
using System.Collections.Generic;
using System.Linq;


namespace Valhalla.EveryDay.Extensions
{
	public static class CollectionExtensions
	{
		private static readonly Random _random = new Random();
		
		
		/// <summary>
		/// Get's random element of the collection.
		/// </summary>
		/// <param name="enumerable">Extended collection</param>
		/// <typeparam name="TValue">Type of values in collection</typeparam>
		/// <returns>Random element</returns>
		/// <exception cref="NullReferenceException">Raised if collection is empty</exception>
		public static TValue GetRandomElement<TValue>(this IEnumerable<TValue> enumerable)
		{
			var list = enumerable.ToList();
			var count = list.Count();
			
			if (count == 0)
				throw new NullReferenceException("Collection has no elements");
 
			return list.ElementAt(_random.Next(count));
		}
	}
}
