using JetBrains.Annotations;


namespace Valhalla.EveryDay.Extensions
{
	public static class DoubleExtensions
	{
		/// <summary>
		/// Clamps value between min and max.
		/// </summary>
		/// <param name="value">Extended value</param>
		/// <param name="min">Lower boundary (included)</param>
		/// <param name="max">Upper boundary (included)</param>
		/// <returns>Clamped value</returns>
		[Pure]
		public static double ClampValue(this double value, double min, double max)
		{
			if (value < min)
				return min;
			
			if (value > max)
				return max;

			return value;
		}
	}
}
