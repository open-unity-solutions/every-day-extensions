using System;
using System.Linq;


namespace Valhalla.EveryDay.Extensions
{
	public static class StringExtensions
	{
		private readonly static Random _random = new Random();
		
		/// <summary>
		/// Transforms string to pascal case:
		/// <br/>
		/// <br/>
		/// my_name -> MyName
		/// </summary>
		/// <param name="str">Extended string</param>
		/// <returns>String converted to pascal case</returns>
		public static string ToPascalCase(this string str)
			=> str
				.Split(new [] {"_"}, StringSplitOptions.RemoveEmptyEntries)
				.Select(s => char.ToUpperInvariant(s[0]) + s.Substring(1, s.Length - 1))
				.Aggregate(string.Empty, (s1, s2) => s1 + s2);
		
		
		
		/// <summary>
		/// Generates random character sequencs, from characters, that are represented in a given string.
		/// <br >
		/// Repeating characters in string are allowed and impacts the probability of their appearance in the result.
		/// </summary>
		/// <param name="str">String with possible characters</param>
		/// <param name="length">Length of the resulted string</param>
		/// <returns></returns>
		public static string GetRandomSequence(this string str, int length)
		{
			var result = string.Empty;
			
			for (var i = 0; i < length; i++)
				result += str[_random.Next(str.Length)];
			
			return result;
		}
	}
}
