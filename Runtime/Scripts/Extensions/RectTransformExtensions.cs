using UnityEngine;
using UnityEngine.UI;


namespace Valhalla.EveryDay.Extensions
{
	public static class RectTransformExtensions
	{
		/// <summary>
		/// ForceRebuilds given RectTransform.
		/// </summary>
		/// <param name="rectTransform">Extended rectTransform</param>
		public static void RebuildLayout(this RectTransform rectTransform)
			=> LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);
		
		
		/// <summary>
		/// Makes anchors to maximum size.
		/// </summary>
		/// <param name="transform">Extended rectTransform</param>
		public static void MaximizeAnchors(this RectTransform transform)
		{
			transform.anchorMin = Vector2.zero;
			transform.anchorMax = Vector2.one;
			transform.anchoredPosition = Vector2.zero;
			transform.offsetMin = Vector2.zero;
			transform.offsetMax = Vector2.zero;
		}
		
		
		/// <summary>
		/// Gets center of the given rect in world space.
		/// </summary>
		/// <param name="rect">Extended rectTransform</param>
		/// <returns>Center of the rect in world space</returns>
		public static Vector2 GetWorldCenter(this RectTransform rect)
		{
			var (left, right, bottom, top) = rect.GetWorldSides();

			return new Vector2(left + (right - left) / 2, bottom + (top - bottom) / 2);
		}


		/// <summary>
		/// Gets bottom-center of the given rect in world space.
		/// </summary>
		/// <param name="rect">Extended rectTransform</param>
		/// <returns>Bottom-center of the rect in world space</returns>
		public static Vector2 GetWorldBottomCenter(this RectTransform rect)
		{
			var (left, right, bottom, _) = rect.GetWorldSides();

			return new Vector2(left + (right - left) / 2, bottom);
		}


		/// <summary>
		/// Gets size of the given rect in world space.
		/// </summary>
		/// <param name="rect">Extended rectTransform</param>
		/// <returns>Size of the rect in world space</returns>
		public static Vector2 GetWorldSize(this RectTransform rect)
		{
			var (left, right, bottom, top) = rect.GetWorldSides();

			return new Vector2(right - left, top - bottom);
		}


		/// <summary>
		/// Gets aspect ration of the given rect.
		/// </summary>
		/// <param name="rect">Extended rectTransform</param>
		/// <returns>Rect's aspect ratio</returns>
		public static float GetAspectRatio(this RectTransform rect)
		{
			var rectData = rect.rect;

			return Mathf.Abs(rectData.width / rectData.height);
		}


		private static (float left, float right, float bottom, float top) GetWorldSides(this RectTransform rect)
		{
			var corners = new Vector3[4];
			rect.GetWorldCorners(corners);

			var bottomLeft = corners[0];
			var topRight = corners[2];

			var left = bottomLeft.x;
			var right = topRight.x;
			var bottom = bottomLeft.y;
			var top = topRight.y;

			return (left, right, bottom, top);
		}
	}
}
