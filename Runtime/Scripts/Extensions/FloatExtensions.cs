using JetBrains.Annotations;


namespace Valhalla.EveryDay.Extensions
{
	public static class FloatExtensions
	{
		/// <summary>
		/// Clamps value between min and max.
		/// </summary>
		/// <param name="value">Extended value</param>
		/// <param name="min">Lower boundary (included)</param>
		/// <param name="max">Upper boundary (included)</param>
		/// <returns>Clamped value</returns>
		[Pure]
		public static float ClampValue(this float value, float min, float max)
		{
			if (value < min)
				return min;
			
			if (value > max)
				return max;

			return value;
		}
	}
}
