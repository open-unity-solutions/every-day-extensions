using System;
using System.Collections.Generic;
using System.Linq;


namespace Valhalla.EveryDay.Extensions
{
	public static class Enum<TEnum>
		where TEnum : struct, Enum
	{
		/// <summary>
		/// Returns collection of all Enum's values
		/// </summary>
		public static IEnumerable<TEnum> Values
			=> Enum.GetValues(typeof(TEnum)).Cast<TEnum>();
	}
}

