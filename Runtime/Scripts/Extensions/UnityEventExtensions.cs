using UnityEngine;
using UnityEngine.Events;


namespace Valhalla.EveryDay.Extensions
{
	public static class UnityEventExtensions
	{
		/// <summary>
		/// Invokes given UnityEvent only if application is started.
		/// </summary>
		/// <param name="unityEvent">Extended event</param>
		public static void InvokeInPlaymode(this UnityEvent unityEvent)
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
			#endif

			unityEvent.Invoke();
		}


		/// <summary>
		/// Invokes given UnityEvent only if application is started.
		/// </summary>
		/// <param name="unityEvent">Extended event</param>
		/// <param name="arg1">1st event argument's</param>
		/// <typeparam name="T1">1st event argument's type</typeparam>
		public static void InvokeInPlaymode<T1>(this UnityEvent<T1> unityEvent, T1 arg1)
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
			#endif

			unityEvent.Invoke(arg1);
		}


		/// <summary>
		/// Invokes given UnityEvent only if application is started.
		/// </summary>
		/// <param name="unityEvent">Extended event</param>
		/// <param name="arg1">1st event argument's</param>
		/// <param name="arg2">2nd event argument's</param>
		/// <typeparam name="T1">1st event argument's type</typeparam>
		/// <typeparam name="T2">2nd event argument's type</typeparam>
		public static void InvokeInPlaymode<T1, T2>(this UnityEvent<T1, T2> unityEvent, T1 arg1, T2 arg2)
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
			#endif

			unityEvent.Invoke(arg1, arg2);
		}


		/// <summary>
		/// Invokes given UnityEvent only if application is started.
		/// </summary>
		/// <param name="unityEvent">Extended event</param>
		/// <param name="arg1">1st event argument's</param>
		/// <param name="arg2">2nd event argument's</param>
		/// <param name="arg3">3rd event argument's</param>
		/// <typeparam name="T1">1st event argument's type</typeparam>
		/// <typeparam name="T2">2nd event argument's type</typeparam>
		/// <typeparam name="T3">3rd event argument's type</typeparam>
		public static void InvokeInPlaymode<T1, T2, T3>(this UnityEvent<T1, T2, T3> unityEvent, T1 arg1, T2 arg2, T3 arg3)
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
			#endif

			unityEvent.Invoke(arg1, arg2, arg3);
		}
	}
}
