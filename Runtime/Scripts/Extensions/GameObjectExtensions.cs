using UnityEngine;


namespace Valhalla.EveryDay.Extensions
{
	public static class GameObjectExtensions
	{
		/// <summary>
		/// Destroys the given object. Of game in editor mode, destroys it immediately.
		/// </summary>
		/// <param name="obj">Extended object, that will be destroyed</param>
		public static void DestroySafely(this GameObject obj)
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying)
				Object.DestroyImmediate(obj);
			else
			#endif
				Object.Destroy(obj);
		}
		
		
		/// <summary>
		/// Destroys all children of the given transform. If in editor mode, destroys them immediately.
		/// </summary>
		/// <param name="transform"></param>
		/// <returns></returns>
		public static void DestroyAllChildren(this Transform transform)
		{
			for (var i = transform.childCount - 1; i >= 0; i--)
			{
				var child = transform.GetChild(i).gameObject;

				child.DestroySafely();
			}
		}
	}
}
