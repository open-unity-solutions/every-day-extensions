## [1.4.8] – 2023-08-30

### Added
- `.GetRandomSequence` for `string`


## [1.4.3] – 2023-07-21

### Fixed
- Fixed incorrect destroy method in `GameObject.DestroySafely`


## [1.4.2] – 2023-07-11

### Added
- `float` extension for clamping
- `double` extension for clamping


## [1.4.1] – 2023-06-30

### Added
- `GameObject` extension with `.DestroyAllChildren` method

### Changed
- CI/CD updated


## [1.4.0]

### Added
- `GameObject` extension with `.DestroySafely` method
- `InstanceHelper`, that helps to instantiate prefab with link to the source asset
- Docstrings to all public API entries

### Changed
- Licence year updated
- Namespace of extensions changed to `Valhalla.EveryDay.Extensions`


## [1.3.0]

### Added
- `RectTransform` extensions for coords and layour rebuild
- `int` extension for clamping
- `UnityEvent` extension for runtime-only events


## [1.2.0]

### Changed
- Rebranded to Valhalla


## [1.1.0]

### Added
- `Collection<T>.GetRandomElement()` – gets random element of given collection
- `TODO` added to changelog


## [1.0.0]

### Added
- Enum enumerator
- String to pascal case converter
