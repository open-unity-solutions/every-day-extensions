### Get any enum values typed

```C#

public enum MyEnum
{
    A,
    B,
    C,
}

...

Debug.Log("Values:")

foreach(MyEnum value in Enum<MyEnum>.Values)
    Debug.Log(value.ToString());
```

Output:
```
Values:
A
B
C
```


### Get random element from collection

```C#

var myList = new List<int>() 
{
    23, 
    42, 
    1337,
};

for (int i = 0; i < 10; i++)
{
    var randomElement = myList.GetRandomElement();  // Every iteration it vould be random number: 23 or 42 or 1337 
    Debug.Log(randomElement);
}

```
Output:\
Will be 10 random values from list: [23, 42, 1337] 

## TODO
- [ ] PascalCase
